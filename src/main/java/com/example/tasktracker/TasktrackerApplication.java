package com.example.tasktracker;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.jca.endpoint.GenericMessageEndpointFactory;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
@EnableWebMvc
@SpringBootApplication
public class TasktrackerApplication extends WebMvcConfigurerAdapter {
    private static final Logger log=LoggerFactory.getLogger( TasktrackerApplication.class );
    public static void main(String[] args) {
        SpringApplication.run( TasktrackerApplication.class, args );

    }
  @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry){
        registry.addResourceHandler(
                "/css/**")
                .addResourceLocations( "classpath:/static/css/" );
    }
    }
/*@Bean
public InternalResourceViewResolver viewResolver(){
        InternalResourceViewResolver viewResolver
                = new InternalResourceViewResolver(  );
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix( ".com.example.tasktracker/resources/templates" );
        viewResolver.setSuffix(".html"  );
        return viewResolver;
}*/



