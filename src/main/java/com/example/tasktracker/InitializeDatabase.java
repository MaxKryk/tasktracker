package com.example.tasktracker;
import com.example.tasktracker.User.User;
import com.example.tasktracker.User.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class InitializeDatabase {
    @Bean // dziala na starcie
    public CommandLineRunner demo(UserRepository repository) {
        return (args) -> {
            // save a couple of customers
            log.info("Preloading " + repository.save(new User("kaybli","Kay", "Blitz", "kay.blitz@test.com")));
            log.info("Preloading " + repository.save(new User("chlbri","Chloe", "O'Brian", "chloe.obrien@test.com")));
            log.info("Preloading " + repository.save(new User("kimbau", "Kim","Bauer", "kim.bauer@test.com")));
            log.info("Preloading " + repository.save(new User("flopar","Florian", "Parmer", "florian.parmer@test.com")));
            log.info("Preloading " + repository.save(new User("condes","Conan", "Dessler", "conan.dessler@test.com")));

            // fetch all customers
            log.info("Customers found with findAll():");
            log.info("-------------------------------");
            for (User user : repository.findAll()) {
                log.info( user.toString());
            }
            log.info("");

            // fetch an individual customer by ident
            repository.findById(1L)
                    .ifPresent( user -> {
                        log.info("User found with findById(1L):");
                        log.info("--------------------------------");
                        log.info( user.toString());
                        log.info("");
                    });

            // fetch customers by last name
            log.info("User found with findByLastName('Bauer'):");
            log.info("--------------------------------------------");
            repository.findByFirstName("Kay").forEach(bauer -> {
                log.info(bauer.toString());
            });
            // for (User bauer : repository.findByLastName("Bauer")) {
            // 	log.info(bauer.toString());
            // }
            log.info("");
        };
    }
}
