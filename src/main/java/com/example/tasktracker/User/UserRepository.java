package com.example.tasktracker.User;

import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
Optional<User> findById(Long id);
List <User> findByFirstName(String firstName);
List <User> findByLastName(String lastName);
List <User> findByLogin(String login);
List <User> findByMail(String mail);


}

/*
    Spring Data JPA also allows you to define other query methods by simply
        declaring their method signature. In the
case of UserRepository, this is shown with a findByLastName() method.*/
