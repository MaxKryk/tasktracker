package com.example.tasktracker.User;

import lombok.Data;

@Data
public class Ident {

    private long id;

    public Ident(long id){
        this.id=id;
    }

    public Ident() {
            }
}
