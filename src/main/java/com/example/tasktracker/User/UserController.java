package com.example.tasktracker.User;


import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Slf4j
@RestController
@SessionAttributes({"ident", "foundUser"})
class UserController {
    Logger logger = Logger.getLogger( "user controller" );
    private final UserRepository userRepository;


    UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/users")
    public ModelAndView findAll(Model model) {
        List<User> allUsers = userRepository.findAll();
        model.addAttribute( "users", allUsers );
        return new ModelAndView( "all_users" );
    }


    @GetMapping("/users/{id}")
    public ModelAndView showOne(@PathVariable Long id, Model model) {
        User foundUser = userRepository.findById( id )
                .orElseThrow( () -> new UserNotFoundException( id ) );
        model.addAttribute( "user", foundUser );

        return new ModelAndView( "user_found_by_id" );
    }
/*
    @GetMapping("/show_user_and_replace/{id}")
    public ModelAndView showAndReplaceOne(@PathVariable Long id, Model model) {
        logger.info( "passed long: " + id );
        long idConvertedToLong = new Long( id );
           logger.info( "id as long: " + idConvertedToLong );
        User foundUser = userRepository.findById( idConvertedToLong )
                .orElseThrow( () -> new UserNotFoundException( idConvertedToLong ) );
        model.addAttribute( "user", foundUser );
        return new ModelAndView( "user_found_by_id" );
    }*/

    @GetMapping("/add_new_user")
    public ModelAndView UserForm(Model model) {
        model.addAttribute( "user", new User() );
        return new ModelAndView( "add_new_user" );
    }

    @PostMapping("/add_new_user")
    public ModelAndView UserSubmit(@ModelAttribute User user) {
        userRepository.save( user );
        return new ModelAndView( "add_new_user_success" );
    }


    @GetMapping("/find_user")
    public ModelAndView searchOneUser(Model model) {
        model.addAttribute( "findUserDto", new FindUserDto() );

        return new ModelAndView( "find_user" );
    }

    @PostMapping("/find_user")
    public ModelAndView submitParameter(@ModelAttribute FindUserDto findUserDto, Model model) {
        logger.info( "user post mapping begins" );
        List<User> foundUsers = new ArrayList<>();

        logger.info( findUserDto.toString() );
        switch (findUserDto.getSearchBy()) {
            case "id":
                logger.info( "Searching by id" );
                Long idToFind=new Long( findUserDto.getSearchCriterion());

//#TODO java.lang.NumberFormatException:

                       userRepository.findById( idToFind ).orElseThrow( () -> new UserNotFoundException( idToFind ) );

                       logger.info( "@PostMapping(\"/find_user\") something went wrong" );

                        break;
            case "firstName":
                logger.info( "Searching by firstName" );
                foundUsers = userRepository.findByFirstName( findUserDto.getSearchCriterion() );
                for (User element : foundUsers) {
                    logger.info( element.toString() );
                }
                break;

            case "lastName":
                logger.info( "resultlist erased" );
                foundUsers = userRepository.findByLastName( findUserDto.getSearchCriterion() );
                for (User element : foundUsers) {
                    logger.info( element.toString() );
                }
                break;
            case "login":
                logger.info( "Searching by login" );
                foundUsers = userRepository.findByLogin( findUserDto.getSearchCriterion() );
                for (User element : foundUsers) {
                    logger.info( element.toString() );
                }
                break;
            case "mail":
                logger.info( "Searching by mail" );
                foundUsers = userRepository.findByMail( findUserDto.getSearchCriterion() );
                for (User element : foundUsers) {
                    logger.info( element.toString() );
                }
                break;
        }
        User foundUser = new User();
        if (foundUsers.size() == 1) {
            foundUser = foundUsers.get( 0 );
            logger.info( foundUser.toString() );
        }//TODO co jesli wynikow jest wiecej
        logger.info( foundUser.toString() );
        model.addAttribute( "foundUser", foundUser );
        long intToFind = foundUser.getId();
        logger.info( "postmapping find_user: id " + foundUser.getId() );
        Ident identIntToFind = new Ident( foundUser.getId() );


        model.addAttribute( "identIntToFind", identIntToFind );
        //TODO
        logger.info( "postmapping find_user: id in Ident: " + identIntToFind.getId() );
        model.addAttribute( "ident", identIntToFind );
        logger.info( "postmapping find_user: return view User Info" );
        UserTemplateToUpdateDTO userTemplateToUpdateDTO=new UserTemplateToUpdateDTO();
        logger.info( "postmapping find_user: userTemplateToUpdateDTO added as model attribute" );
model.addAttribute( "newUserTemplateToUpdateDTO", userTemplateToUpdateDTO);
        return new ModelAndView( "user_info" );

    }

    @PostMapping("/delete_user_info")
    //TODO widok
    public String PostDeleteUserInfo(@ModelAttribute Ident ident, Model model) {
        logger.info( "POST /delete_user_info passed ident: " + ident.getId() );
        userRepository.deleteById( ident.getId() );
        logger.info( "POST /delete_user_info User with the id : " + ident.getId() + " has been deleted" );
        return new String( "user deleted" );
    }


    @PostMapping("/update_user_info")
    public String PostUpdateUserInfo(@ModelAttribute Ident ident, Model model, UserTemplateToUpdateDTO userTemplateToUpdateDTO) {
        logger.info( "@PostMapping(update_user_info): passed ident: " + ident.getId() );
        logger.info( "@PostMapping(update_user_info): userTemplateToUpdateDTO passed as object: " + userTemplateToUpdateDTO.toString() );
       User foundUser=userRepository.findById( ident.getId()).orElseThrow( ()-> new UserNotFoundException( ident.getId() ) );
        logger.info( "@PostMapping(update_user_info): unchanged entry: " + foundUser.toString());
foundUser.setLogin( userTemplateToUpdateDTO.getLogin() );
        foundUser.setFirstName( userTemplateToUpdateDTO.getFirstName() );
foundUser.setLastName( userTemplateToUpdateDTO.getLastName() );
foundUser.setMail( userTemplateToUpdateDTO.getMail() );
        logger.info( "@PostMapping(update_user_info): changed entry: " + foundUser.toString());
        //#TODO Entry is not changed
        userRepository.save( foundUser );
        return new String( "update user" );
    }
}