package com.example.tasktracker.User;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Data
@Entity
public class User {


    private  @Id @GeneratedValue Long id;
    private String firstName;
    private String lastName;
    private String login;
    private String mail;
    //private String password;

    protected User() {}

    public User(String login, String firstName, String lastName, String mail) {
        this.login=login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail=mail;

    }

    //KONSTRUKTOR ROZSZERZONY


//    public String toStringEnhanced() {
//        return "User{" +
//                '}';
//    }

    @Override
    public String toString() {
        return String.format(
                "User[id=%d, login='%s', firstName='%s', lastName='%s', mail='%s']",
                id, login, firstName, lastName, mail);
    }

}