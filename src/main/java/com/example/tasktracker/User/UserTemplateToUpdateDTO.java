package com.example.tasktracker.User;

import lombok.Data;

@Data
public class UserTemplateToUpdateDTO {
    private String firstName;
    private String lastName;
    private String login;
    private String mail;
}
