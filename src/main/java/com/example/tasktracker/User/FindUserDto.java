package com.example.tasktracker.User;

import lombok.Data;

@Data
public class FindUserDto {

    private String searchCriterion;
    private String searchBy;
    private long id;
    private String deleteOrUpdate;
    public FindUserDto() {

    }



}