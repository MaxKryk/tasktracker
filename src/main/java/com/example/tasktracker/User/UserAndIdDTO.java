package com.example.tasktracker.User;

import lombok.Data;

@Data
public class UserAndIdDTO {
    private User user;
    private Ident ident;
}
